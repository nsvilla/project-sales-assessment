## Introduction

This project contain the following endpoints.

- Login - non token required
- Registrer - super admin token required
- Create Sale - normal user token required
- Get All Sales - admin token required
- Get Sale by Radicated - admin token required
- Edit Sale - admin token required
- Delete Sale - admin token required


## Project Settings
1. The database must be served in sql server.
    - Create a new database with the name **WideWorldImporters**.
    - Run the **ScriptDB.sql** file to create all tables.
    - the database needs to be configured from the file **appsettings.json**.
    
2. Use the postman file to get the enpoint collection

3. Follow the steps below to start the server
```
cd project-sales
dotnet build
dotnet run
```
4. To config hangfire job:
    - In the file **./Startup.cs** line 102 you can modify the job schedule
    - In the file **./controllers/SalesController.cs** line 72 you can modify the email where the message is sent

5. When a record is edited, please verify the idSale attrib in the sales details match with the current sales id, otherwise an error will occur.

6. Please Login with the super admin user to create other users
    - User: admin
    - Password: admin*123