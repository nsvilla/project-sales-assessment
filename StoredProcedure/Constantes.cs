﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSales.Controllers
{
    public class Constantes
    {
        public const int ESTADOACTIVO = 1;
        public const int ESTADOINACTIVO = 2;

        public const int USUARIOADMINISTRADOR = 1;
        public const int USUARIONORMAL = 2;
    }
}
